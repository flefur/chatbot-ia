import ChatGPTService from '../src/services/ChatGPTService.js';
import UberService from '../src/services/UberService.js';
import WeatherService from '../src/services/WeatherService.js';
import LocalStorage from '../src/services/utils/LocalStorage.js';

document.addEventListener('DOMContentLoaded', (event) => {
  const messageForm = document.getElementById('message-input');
  const messageInput = document.getElementById('input-message');
  const messageHistory = document.getElementById('message-history');
  const users = document.querySelectorAll('.user');

  let selectedBot = 'all';
  const botHistories = {
    chatgpt: [],
    uber: [],
    weather: [],
    all: []
  };

  // Event Listener pour l'envoi de messages
  messageForm.addEventListener('submit', function(e) {
    e.preventDefault();
    const messageText = messageInput.value.trim();
    if (messageText && selectedBot !== null) {
      const timestamp = new Date().toLocaleTimeString();
      addMessage('Me', messageText, 'right', selectedBot, timestamp);
      messageInput.value = '';
      processMessage(messageText, selectedBot, timestamp);
    }
  });

  // Event Listener pour la sélection des utilisateurs
  users.forEach(user => {
    user.addEventListener('click', function() {
      users.forEach(u => u.classList.remove('selected'));
      user.classList.add('selected');
      selectedBot = user.dataset.id;
      displayMessageHistory(selectedBot);
    });
  });

  // Fonction pour ajouter un message à l'historique
  function addMessage(sender, text, align, botId, timestamp) {
    const messageElement = document.createElement('div');
    messageElement.classList.add('message', align);
    messageElement.innerHTML = `
      <div class="message right">
      <img src="https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg" alt="User Avatar">
      <div>
        <strong>${sender}</strong>
        <span>${new Date().toLocaleTimeString()}</span>
        <p>${text}</p>
      </div>
      </div>
    `;
    if (botId === selectedBot) {
      messageHistory.appendChild(messageElement);
      messageHistory.scrollTop = messageHistory.scrollHeight;
    }
    LocalStorage.saveMessage(sender, text, botId, timestamp, align);
    botHistories[botId].push({ sender, text, timestamp, align });
  }

  // Fonction pour afficher l'historique des messages d'un bot
  function displayMessageHistory(botId) {
    messageHistory.innerHTML = '';
    botHistories[botId].forEach(msg => {
      const messageElement = document.createElement('div');
      messageElement.classList.add('message', msg.align);
      messageElement.innerHTML = `
        <img src="https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg" alt="User Avatar">
        <div>
          <strong>${msg.sender}</strong>
          <span>${new Date().toLocaleTimeString()}</span>
          <p>${msg.text}</p>
        </div>
      `;
      messageHistory.appendChild(messageElement);
    });
    messageHistory.scrollTop = messageHistory.scrollHeight;
  }

  // Fonction pour traiter les messages et déclencher des actions de bots
  function processMessage(message, botId, timestamp) {
    if (botId === 'chatgpt') {
      callChatGPTService(message, timestamp);
    } else if (botId === 'uber') {
      callUberService(message, timestamp);
    } else if (botId === 'weather') {
      callWeatherService(message, timestamp);
    } else if (botId === 'all') {
      callAllBotsService(message, timestamp)
    }
  }

  // Fonction pour appeler le service ChatGPT
  function callChatGPTService(message, timestamp) {
    if (message === 'help') {
      addMessage('ChatGPT Bot', `Commandes : getTime, explainCapitalism, explainVoteRight`, 'left', selectedBot, timestamp);
    } else {
      ChatGPTService.getResponse(message)
        .then(response => {
          addMessage('ChatGPT Bot', response, 'left', selectedBot, timestamp);
        })
        .catch(error => {
          addMessage('ChatGPT Bot', 'Error fetching response', 'left', selectedBot, timestamp);
        });
    }
  }

  // Fonction pour appeler le service Uber
  function callUberService(message, timestamp) {
    if (message === 'help') {
      addMessage('Uber Bot', `Commandes : getTimeBeforeDriverArrives, getCourse, getDriversAround`, 'left', selectedBot, timestamp);
    } else {
      UberService.getEstimates(message)
        .then(response => {
          addMessage('Uber Bot', `Estimated cost: ${response.estimate}`, 'left', selectedBot, timestamp);
        })
        .catch(error => {
          addMessage('Uber Bot', 'Error fetching estimate', 'left', selectedBot, timestamp);
        });
    }
  }

  // Fonction pour appeler le service Weather
  function callWeatherService(message, timestamp) {
    if (message === 'help') {
      addMessage('Weather Bot', `Commandes : getWeatherAtMyLocation, getWeatherToday, getWeatherForNextWeek`, 'left', selectedBot, timestamp);
    } else {
      WeatherService.getWeather(message)
        .then(response => {
          addMessage('Weather Bot', `Current weather: ${response.weather}`, 'left', selectedBot, timestamp);
        })
        .catch(error => {
          addMessage('Weather Bot', 'Error fetching weather', 'left', selectedBot, timestamp);
        });
    }
  }

  // Fonction pour appeler le service AllBots
  function callAllBotsService(message, timestamp) {
    if (message === 'help') {
      callChatGPTService(message, timestamp)
      callUberService(message, timestamp)
      callWeatherService(message, timestamp)
    }
  }

  // Stockage local
  function loadMessagesFromLocalStorage() {
    const messages = LocalStorage.loadMessages();
    for (const botId in messages) {
      if (messages.hasOwnProperty(botId)) {
        messages[botId].forEach(msg => {
          botHistories[botId].push({ sender: msg.sender, text: msg.text, timestamp: msg.timestamp, align: msg.align });
        });
      }
    }
    if (selectedBot !== null) {
      displayMessageHistory(selectedBot);
    }
  }

  loadMessagesFromLocalStorage();
});