const LocalStorage = {
    saveMessage: function(sender, text, botId, timestamp, align) {
      const messages = JSON.parse(localStorage.getItem('messages')) || {};
      if (!messages[botId]) {
        messages[botId] = [];
      }
      messages[botId].push({ sender, text, timestamp, align });
      localStorage.setItem('messages', JSON.stringify(messages));
    },
  
    loadMessages: function() {
      return JSON.parse(localStorage.getItem('messages')) || {};
    }
  };
  
  export default LocalStorage;
  