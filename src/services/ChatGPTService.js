const ChatGPTService = {
    getResponse: async function(message) {
      if (message === "hello") {
        return "Hello !"
      } else if (message === "Comment ça va aujourd'hui ?") {
        return "ça va super et toi ?"
      } else if (message === "j'adore les crêpes") {
        return "c'est vrai que c'est la meilleure spécialité du monde !"
      } else {
        try {
          const response = await fetch('https://api.openai.com/v1/chat/completions', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer TOKEN_TO_BE_PUT_HERE`
            },
            body: JSON.stringify({
              model: 'gpt-3.5-turbo',
              messages: [{ role: 'user', content: message }]
            })
          });
  
          const data = await response.json();
          console.log(data)
          return data.choices[0].text.trim();
        } catch (error) {
          console.error('Error fetching response from ChatGPT:', error);
          throw error;
        }
      }
    }
  };
  
  export default ChatGPTService;
  