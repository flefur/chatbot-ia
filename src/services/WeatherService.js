const WeatherService = {
    getWeather: async function(city) {
      if (message === "hello") {
        return "Hello !"
      } else if (message === "Comment ça va aujourd'hui ?") {
        return "ça va super et toi ?"
      } else if (message === "j'adore les crêpes") {
        return "c'est vrai que c'est la meilleure spécialité du monde !"
      } else { 
        try {
          const response = await fetch(`https://api.openweathermap.org/data/3.0/onecall?lat=33.44&lon=-94.04&exclude=hourly,daily&appid={TOKEN}`);
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          const data = await response.json();
          return data;
        } catch (error) {
          console.error('Error fetching weather data:', error);
          throw error;
        }
      }
    }
  };
  
  export default WeatherService;
  