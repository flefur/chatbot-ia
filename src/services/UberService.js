const UberService = {
    getEstimates: async function(startLatitude, startLongitude, endLatitude, endLongitude) {
      
      if (message === "hello") {
        return "Hello !"
      } else if (message === "Comment ça va aujourd'hui ?") {
        return "ça va super et toi ?"
      } else if (message === "j'adore les crêpes") {
        return "c'est vrai que c'est la meilleure spécialité du monde !"
      } else { 
        try {
          const response = await fetch('https://api.uber.com/v1.2/estimates/price', {
            method: 'GET',
            headers: {
              'Authorization': `Token uber-token`,
              'Content-Type': 'application/json'
            },
            params: {
              start_latitude: startLatitude,
              start_longitude: startLongitude,
              end_latitude: endLatitude,
              end_longitude: endLongitude
            }
          });
  
          const data = await response.json();
          if (data.prices && data.prices.length > 0) {
            return data.prices[0].estimate;
          } else {
            throw new Error('No price estimates available');
          }
        } catch (error) {
          console.error('Error fetching price estimate from Uber:', error);
          throw error;
        }
      }
    }
  };
  
  export default UberService;
  